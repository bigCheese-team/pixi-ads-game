import StairContainer from './stair/stairContainer/stairContainer.js'

let Application			= PIXI.Application,
	Container			= PIXI.Container,
	loader				= PIXI.loader,
	resources			= PIXI.loader.resources,
	Sprite				= PIXI.Sprite



let app = window.app =  new Application({
	width: 1390,
	height: 640,
	antialias: true,
	transparent: true
});
document.body.appendChild( app.view );



let  gameScene, spriteOne, spriteTwo, bg, stairsToolsContainer,
	toolsPosition = { x: app.screen.width /1.8, y: app.screen.height / 2 }

loader
	.add("sprites/sprites-3-1.json")
	.add("sprites/sprites-3-0.json")
	.load( setup );

function setup() {

	const sprites = window.sprites = {
		...resources["sprites/sprites-3-0.json"].textures,
		...resources["sprites/sprites-3-1.json"].textures
	}

	gameScene = new Container()
	gameScene.sortableChildren = true
	app.stage.addChild( gameScene )

	bg = new Sprite( sprites["room.jpg"] )
	bg.anchor.set(0.48, 0.6)
	bg.x = app.screen.width / 2
	bg.y = app.screen.height / 2
	gameScene.addChild( bg )

	let austin = new Sprite( sprites["test_task_0003_Austin.png"])
	austin.x = app.screen.width / 1.8
	austin.y = app.screen.height / 6
	gameScene.addChild(austin)

	let logo = new Sprite( sprites["test_task_0001_logo.png"])
	logo.x = app.screen.width / 20
	logo.y = 5
	gameScene.addChild(logo)

	let table = new Sprite( sprites['test_task_0003s_0005_table.png'])
	table.x = app.screen.width / 6
	table.y = app.screen.height / 3
	gameScene.addChild(table)

	let globe = new Sprite( sprites['test_task_0003s_0003_globe.png'])
	globe.x = app.screen.width / 11
	globe.y = app.screen.height / 5
	gameScene.addChild(globe)

	let growth = new Sprite( sprites['growth-1.png'])
	growth.x = app.screen.width - 200
	growth.y = app.screen.height - 200
	growth._zIndex = 10000
	gameScene.addChild(growth)

	let plant = new Sprite(sprites['test_task_0003s_0004_plant.png'])
	plant.x = app.screen.width / 1.3
	plant.y = app.screen.height  / 2.7
	let plant1 = new Sprite(sprites['test_task_0003s_0004_plant.png'])
	plant1.x = app.screen.width / 3
	plant1.y = 0
	gameScene.addChild(plant, plant1)

	let bookStand = new Sprite( sprites["test_task_0003s_0002_book_stand.png"])
	bookStand.x = app.screen.width / 1.5
	bookStand.y = app.screen.height / 8
	gameScene.addChild(bookStand)

	let sofa = new Sprite( sprites["test_task_0003s_0001_Layer-1.png"])
	sofa.x = app.screen.width / 9
	sofa.y = app.screen.height / 2
	gameScene.addChild( sofa )


	/** Display stair **/
	const stairContainer = new StairContainer(app, {spriteOne, spriteTwo})
	gameScene.addChild( stairContainer )



	/** Tools container for stair **/
	stairsToolsContainer = new Container()
	stairsToolsContainer.position.set(toolsPosition.x, toolsPosition.y)
	gameScene.addChild(stairsToolsContainer)

	//state = play;
	//app.ticker.add( () => play() );

}

