
const Sprite = PIXI.Sprite

import RepairContainer from '../repairStair/repairContainer/repairContainer.js'

export default  class StairContainer extends PIXI.Container {
    constructor(area, sprites) {
        super()
        this.sprites = window.sprites
        this.app = window.app
        this.x = this.app.screen.width / 1.53
        this.y = this.app.screen.height / 15
        this.stairPosition = { x: this.width / 1.8 , y: this.height / 1.0}

        this.oldStair = new Sprite( window.sprites["test_task_0002s_0003_old_stair.png"] )
        this.oldStair.interactive = true;
        this.oldStair.name = "STAIR"
        this.oldStair.x = this.stairPosition.x
        this.oldStair.y = this.stairPosition.y
        this.addChild( this.oldStair )
        const repairContainer = new RepairContainer(this, sprites)
        this.addChild(repairContainer)

    }



}

