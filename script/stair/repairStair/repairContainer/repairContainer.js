
const Container = PIXI.Container
const Sprite = PIXI.Sprite
let ticker = PIXI.Ticker.shared;

import StairTools from "../stairTools/stairTools.js";

export default class RepairContainer extends Container {
    constructor(area) {
        super()
        this.app = window.app
        this.area = area
        this.sprites = window.sprites
        this.hammer = new Sprite(window.sprites['test_task_0002_icon_hammer.png'])
        this.hammer.visible = false
        this.hammer.x = area.width / 2.5
        this.hammer.y = area.height / 2.3
        this.hammer.interactive = true;
        this.hammer.buttonMode = true;
        this.addChild( this.hammer )
        this.hammer.on("pointerdown", this.clickToHammer)
        this.displayHummer()
    }

    clickToHammer = () => {
        this.hammer.destroy()
        ticker.stop()
        return new StairTools(this.area)
    }

    displayHummer = () => {
        let timer = 0
        let factor = 1
        let count = 2000
        ticker.add(() => {
            if(timer >= 100) {
                this.hammer.visible = true
                ~~count  === 2000? factor =  1 : null
                ~~count === 2100? factor =  -1 : null
                count += factor
                this.hammer.scale.x = Math.sin(count / 1000);
                this.hammer.scale.y = Math.sin(count / 1000);
            } else {
                timer++
            }
        })
    }
}