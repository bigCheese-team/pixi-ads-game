const Container = PIXI.Container
const Sprite = PIXI.Sprite
import ButtonOkContainer from "../buttonOk/buttonOkContainer.js";


export default class StairTools extends Container {
  constructor (area) {
      super()
      this.area = area
      this.sprites = window.sprites
      this.stairTools = [
            {sprite: 'test_task_0001s_0002_03.png', position: {x: -70, y: -20}, toolItemContainer: '',
                stairSprite:'test_task_0002s_0000_new_stair_03.png', isInit:false, isChoose:false},
            {sprite: 'test_task_0001s_0000_01.png', position: {x: 60, y: -20}, toolItemContainer: '',
                stairSprite:'test_task_0002s_0002_new_stair_01.png', isInit:false, isChoose:false},
            {sprite: 'test_task_0001s_0001_02.png', position: {x: 190, y: -20}, toolItemContainer: '',
                stairSprite:'test_task_0002s_0001_new_stair_02.png', isInit:false, isChoose:false}
        ]

        this.area.addChild(this)

        this.createStairButtons()
    }

    createStairButtons = () => {
        this.stairTools.forEach((elem, i) => {
            elem.toolItemContainer = new Container()
            elem.toolItemContainer.position.set(
                this.width / 2 + elem.position.x,
                this.height / 2 + elem.position.y
            )

            elem.toolItemContainer.interactive = true;
            elem.toolItemContainer.buttonMode = true;

            this.area.addChild(elem.toolItemContainer)

            const toolBg = new Sprite(this.sprites['test_task_0001s_0007_1.png'])
            toolBg.x = elem.toolItemContainer.width / 2
            toolBg.y = elem.toolItemContainer.height / 2
            elem.toolItemContainer.addChild(toolBg)

            const toolStair = new Sprite(this.sprites[elem.sprite])

            toolStair.x = elem.toolItemContainer.width / 2 - toolStair.width / 2 + 20
            elem.toolItemContainer.addChild(toolStair)

            this.buttonOkContainer = new ButtonOkContainer(this.area, this.stairTools)
            elem.toolItemContainer.on("pointerup", () => this.clickToTool(elem, i))
        })
    }

    clickToTool = async (button, id, buttonOkContainer) => {

        this.stairTools.forEach(item => item.isChoose = false)
        this.buttonOkContainer.buttonSprite.x = button.position.x
        this.buttonOkContainer.buttonSprite.y = button.position.y + this.buttonOkContainer.buttonSprite.height + 30

        if(this.stairTools[id].isChoose) {
                this.stairTools[id].isChoose = false
            } else {
                this.stairTools[id].isChoose = true
            }

        this.buttonOkContainer.buttonSprite.visible = this.stairTools[id].isChoose

        this.area.children.forEach(elem => {

            if(elem.name === "STAIR") {
                elem.y = elem.y - 50
                elem.texture = this.sprites[button.stairSprite]

                setTimeout(() => {  elem.y = elem.y + 50; }, 100);
                let timer = 10
            }
        })
    }
}